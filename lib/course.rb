class Course
  def initialize(name, department, credits, days=nil, time_block=nil)
    @name = name
    @department = department
    @credits = credits
    @days = days
    @time_block = time_block
    @students = []
  end

  attr_accessor :name, :department, :credits, :days, :period, :students, :days, :time_block

  def add_student(student)
    unless @students.include?(student)
      @students << student
      student.enroll(self)
    end
  end

  def conflicts_with?(other_course)
    self.days.each do |day|
      if other_course.days.include?(day)
        if self.time_block == other_course.time_block
          return true
        end
      end
    end

    false
  end
end
