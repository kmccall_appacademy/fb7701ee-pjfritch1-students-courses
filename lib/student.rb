class Student
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  attr_accessor :first_name, :last_name, :courses

  def name
    "#{self.first_name} #{self.last_name}"
  end


  def enroll(course)
    if self.has_conflict?(course)
      raise "time slot filled"
    end

    unless @courses.include?(course)
      @courses << course
      unless course.students.include?(self)
        course.students << self
      end
    end
  end


  def course_load
    courseLoad = Hash.new(0)
    @courses.each {|course| courseLoad[course.department] += course.credits}
    courseLoad
  end

  def has_conflict?(new_course)
    @courses.each {|course| return true if course.conflicts_with?(new_course)}

    false
  end
end
